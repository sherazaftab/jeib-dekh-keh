//
//  TViewController.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import UIKit

class TViewController: UIViewController {

    
    var transaction = [Transaction]()
    
    @IBOutlet weak var icomeLabel: UILabel!
    
    @IBOutlet weak var expenseLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        transaction.append(Transaction.transaction1)
        transaction.append(Transaction.transaction2)
        transaction.append(Transaction.transaction3)
        transaction.append(Transaction.transaction4)
        
        
        var totalIncome: Double = 0.0
        var totalExpense: Double = 0.0
        var balance: Double = 0.0
        
        transaction.forEach { transaction in
            switch transaction.transactionType {
            case.income:
                totalIncome += transaction.amount
            case.expense:
                totalExpense += transaction.amount
            case.transfer:
                break
            }
            
        }
        balance = totalIncome-totalExpense
        
        icomeLabel.text = String(totalIncome)
        expenseLabel.text = String(totalExpense)
        balanceLabel.text = String(balance)
        
        
    }
  
}
