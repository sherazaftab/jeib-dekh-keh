//
//  MoreViewController.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import UIKit

class MoreViewController: UIViewController {
 
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var budgetNameLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        userNameLabel.text = More.household.User_Name
        budgetNameLabel.text = More.household.Budget_Name
    }
    
  
   
}
