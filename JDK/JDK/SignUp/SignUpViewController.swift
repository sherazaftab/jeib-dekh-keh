//
//  SignUpViewController.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import UIKit

class SignUpViewController: UIViewController {
    let user: User = User.sheraz
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTestField: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        if firstNameTextField.text==user.firstName,
           lastNameTestField.text==user.lastName {
            let alert = UIAlertController(title: "Done", message: "Correct First and Last Name", preferredStyle: .alert)
            
            let action=UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Error", message: "Wrong Email/Passowrd", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
            
            
            
        }
        
        
        
    }
    
    

}
