//
//  CategoryModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

struct Category {
    var CID: String
    var UID: String
    var categoryName: String
}

extension Category {
    static var bowling: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Bowling")
    }
    
    static var clothes: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Clothes")
    }
    
    static var food: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Food")
    }
    
    static var electricity: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Electricity")
    }
    
    static var pharmacy: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Pharmacy")
    }
    
    static var carLoan: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Car Loan")
    }
    
    static var pet: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Pet")
    }
    
    static var gas: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Gas")
    }
    
    static var insurance: Category {
        Category(CID: "1", UID: User.sheraz.UID, categoryName: "Insurance")
    }
    
}
