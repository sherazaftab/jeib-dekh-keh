//
//  MoreModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

struct More{
    
    let UID: String
    let User_Name: String
    let Budget_Name: String
    
}
extension More{
    
    static var household: More{
        More(UID: User.sheraz.UID, User_Name: "Sheraz Aftab", Budget_Name: "My HouseHold")
    }
    
}
