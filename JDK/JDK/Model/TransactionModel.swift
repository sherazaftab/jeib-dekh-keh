//
//  TransactionModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

enum TransactionType {
    case income
    case expense
    case transfer
}

struct Transaction {
    var UID: String
    var amount: Double
    var categories: [Category]
    var transactionDate: Date
    var transactionNote: String?
    var transactionType: TransactionType
}

extension Transaction {
    static var transaction1: Transaction {
        Transaction(UID: User.sheraz.UID, amount: 500.0, categories: [Category.carLoan], transactionDate: Date(), transactionNote: nil, transactionType: .expense)
    }
    static var transaction2: Transaction {
        Transaction(UID: User.sheraz.UID, amount: 4000.0, categories: [Category.carLoan], transactionDate: Date(), transactionNote: nil, transactionType: .income)
    }
    static var transaction3: Transaction {
        Transaction(UID: User.sheraz.UID, amount: 300.0, categories: [Category.carLoan], transactionDate: Date(), transactionNote: nil, transactionType: .expense)
    }
    static var transaction4: Transaction {
        Transaction(UID: User.sheraz.UID, amount: 200.0, categories: [Category.carLoan], transactionDate: Date(), transactionNote: nil, transactionType: .income)
    }
}


