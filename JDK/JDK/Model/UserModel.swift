//
//  UserModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

struct User {
    let UID: String
    let firstName: String
    let lastName: String
    let email:String
    let passWord:String
    let dob: Date
    
}

extension User {
    static var sheraz: User {
        User(UID: "1",
             firstName: "Sheraz",
             lastName: "Aftab",
             email:"Sheraz.aftab0077@gmail.com"
             ,passWord: "hiThere",
             dob: Date())
    }
}
