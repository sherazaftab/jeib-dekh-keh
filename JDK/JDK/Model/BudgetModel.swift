//
//  BudgetModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

struct Budget {
    var BID: String
    var UID: String
    var budgetGoal: Double
    var budgetName: String
    var budgetRemaining: Double
}

extension Budget {
    static var householdBudget: Budget {
        Budget(BID: "1",
               UID: User.sheraz.UID,
               budgetGoal: 1000.0,
               budgetName: "Household",
               budgetRemaining: 450.0)
    }
    
    static var commuteBudget: Budget {
        Budget(BID: "2",
               UID: User.sheraz.UID,
               budgetGoal: 500.0,
               budgetName: "Commute",
               budgetRemaining: 475.0)
    }
}
