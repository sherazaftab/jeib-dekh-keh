//
//  FundModel.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import Foundation

struct Fund {
    var FID: String
    var UID: String
    var fundName: String
    var fundGoal: Double
    var fundSavings: Double
}


extension Fund {
    static var tripFund: Fund {
        Fund(FID: "1", UID: User.sheraz.UID, fundName: "Trip", fundGoal: 1000.0, fundSavings: 100.0)
    }
    
    static var bikeFund: Fund {
        Fund(FID: "1", UID: User.sheraz.UID, fundName: "New Bike", fundGoal: 800.0, fundSavings: 65.0)
    }
}
