//
//  ViewController.swift
//  JDK
//
//  Created by Sheraz Aftab on 09/03/2022.
//

import UIKit

enum UserLoginState {
    case SignUp
    case SignIn
    case TabBar
}

class ViewController: UIViewController {

    let initialNavigation: UserLoginState = .TabBar
    
    let categories: [Category] = [Category.bowling, Category.carLoan, Category.electricity]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            switch self.initialNavigation {
            case .TabBar:
                self.performSegue(withIdentifier: "TabBarSegueID", sender: nil)
            case .SignUp:
                self.performSegue(withIdentifier: "SignUpSegueID", sender: nil)
            case .SignIn:
                self.performSegue(withIdentifier: "SignInSegueID", sender: nil)
            }
        }
        
    }


}

