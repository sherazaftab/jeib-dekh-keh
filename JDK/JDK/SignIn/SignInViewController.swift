//
//  SignInViewController.swift
//  JDK
//
//  Created by Sheraz Aftab on 15/03/2022.
//

import UIKit

class SignInViewController: UIViewController {

    //MARK: Variables
    let user: User = User.sheraz
    
    //MARK: Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passWordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        
    }
    

    @IBAction func signInAction(_ sender: Any) {
        if emailTextField.text == user.email,
           passWordTextfield.text == user.passWord {
            // Passed
            let alert = UIAlertController(title: "Done", message: "Correct Email/Passowrd", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Error", message: "Wrong Email/Passowrd", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            // Failed
        }
    }
    

}
